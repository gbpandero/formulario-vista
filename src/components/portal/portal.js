import React from 'react';
import FormComponent from '../form/form';
import image from './propuesta-de-formulario.png' 

class PortalComponent extends React.Component {
  render() {
    return (
      <div class="flex">
        <div class="portal-image">
          <img src={image} alt={"imagen"}/>
        </div>
        <div class="portal-form">
          <div class="portal-form__container">
            <div class="color-grey portal-form__container--bold">Actualiza tus datos y participa en el sorteo de </div>
            <div class="color-blue portal-form__container--subtitle">4 premios de S/ xxxx</div>
            <div class="color-grey">!Si ganas, nos comunicaremos contigo por medio de estos datos!</div>
            <FormComponent></FormComponent>              
          </div>
        
        </div>

      </div>
    );
  }
}

export default PortalComponent;
