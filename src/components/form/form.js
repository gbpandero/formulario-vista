import React from 'react';

class FormComponent extends React.Component {
  constructor() {
    super();
  }

  done() {
    console.log('hecho')
  }

  render() {
    return (
      <form>
        <div>
          <input class="color-grey" type="text" placeholder="Tipo de documento" required/>
          <input class="color-grey" type="text" placeholder="Documento de identidad" required/>         
        </div>

        <input class="color-grey" type="text" placeholder="correo electrónico" required/>
        <input class="color-grey" type="text" placeholder="celular" required/>

        <input type="button" value="Subscribe!" onClick={this.done}/>
      </form>
    );
  }
}

export default FormComponent;