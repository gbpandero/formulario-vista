import React from 'react';
import './App.css';
import PortalComponent from './portal/portal.js';

function App() {
  return (
    <div className="App">
      <PortalComponent></PortalComponent>
    </div>
  );
}

export default App;
